﻿Imports System.Text
Imports System.IO
Imports System.Net
Imports System.Collections.Specialized
Imports System.Environment

Public Class frm_main
    Private Sub frm_main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Directory.SetCurrentDirectory(Application.StartupPath)
        Dim Arguments = Environment.GetCommandLineArgs()
        If (Arguments.Length > 1 And My.Settings.Username <> "" And My.Settings.Password <> "" And My.Settings.UploadDir <> "") Then
            UploadFile(Arguments(1))
            If (My.Settings.AutoShare) Then
                Dim Response = ShareFile(My.Settings.UploadDir + Path.GetFileName(Arguments(1)))
                Dim ResponseParsed = XDocument.Parse((New Text.UTF8Encoding).GetString(Response))

                Clipboard.SetText(ResponseParsed.Element("ocs").Element("data").Element("url").Value)
                MsgBox(Path.GetFileName(Arguments(1)) + " has been uploaded to Cloud" & vbNewLine & "Public link has been copied to your clipboard")
            Else
                MsgBox(Path.GetFileName(Arguments(1)) + " has been uploaded to Cloud")
            End If
            Me.Close()
        ElseIf (Arguments.Length > 1 And (My.Settings.Username <> "" Or My.Settings.Password <> "" Or My.Settings.UploadDir <> "")) Then
            MsgBox("Please open the tool first and set your preferences!")
        End If
        tbx_user.Text = My.Settings.Username
        tbx_password.Text = My.Settings.Password
        tbx_uploaddir.Text = My.Settings.UploadDir
        cbx_autoshare.Checked = My.Settings.AutoShare

        If (My.Computer.Registry.CurrentUser.OpenSubKey("Software\Classes\*\shell\Upload to Heskcloud") IsNot Nothing) Then
            btn_context_remove.Enabled = True
        Else
            btn_context_add.Enabled = True
        End If

        If (System.IO.File.Exists(GetFolderPath(SpecialFolder.ApplicationData) + "\Microsoft\Windows\SendTo\Heskcloud.lnk")) Then
            btn_sendto_remove.Enabled = True
        Else
            btn_sendto_add.Enabled = True
        End If
    End Sub

    Function UploadFile(FilePath)
        Dim WebClient = New System.Net.WebClient()
        Dim Credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(My.Settings.Username + ":" + My.Settings.Password))
        Dim UploadDir = My.Settings.DavUrl + My.Settings.Username + My.Settings.UploadDir + Path.GetFileName(FilePath)
        Dim FileBytes = My.Computer.FileSystem.ReadAllBytes(FilePath)

        WebClient.Headers.Add("Authorization", "Basic " + Credentials)
        'WebClient.Headers.Add("Content-Type", "image/jpeg")

        WebClient.UploadData(UploadDir, "PUT", FileBytes)
        Return WebClient.ResponseHeaders.ToString()
    End Function

    Function ValidateCredentials()
        Try
            Dim WebClient = New System.Net.WebClient()
            Dim Credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(My.Settings.Username + ":" + My.Settings.Password))
            Dim UploadDir = My.Settings.DavUrl + My.Settings.Username

            WebClient.Headers.Add("Authorization", "Basic " + Credentials)

            WebClient.DownloadString(UploadDir)

            Return "Login successful" & vbNewLine & "Settings have been saved"
        Catch
            Return "Login failed" & vbNewLine & "Settings have been saved"
        End Try
    End Function

    Function ShareFile(CloudPath)
        Dim WebClient = New System.Net.WebClient()
        Dim Credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(My.Settings.Username + ":" + My.Settings.Password))
        Dim POSTData As New NameValueCollection()

        WebClient.Headers.Add("Authorization", "Basic " + Credentials)
        WebClient.Headers.Add("OCS-APIRequest", "true")
        POSTData.Add("path", CloudPath)
        POSTData.Add("shareType", "3")

        Dim Response = WebClient.UploadValues(My.Settings.FileShareApiUrl, "POST", POSTData)
        Return Response
    End Function

    Function CreateShortcut(ByVal TargetName As String, ByVal ShortcutPath As String, ByVal ShortcutName As String) As Boolean
        Dim oShell As Object
        Dim oLink As Object

        Try
            oShell = CreateObject("WScript.Shell")
            oLink = oShell.CreateShortcut(ShortcutPath & "\" & ShortcutName & ".lnk")

            oLink.TargetPath = TargetName
            oLink.WindowStyle = 1
            oLink.Save()

            Return True
        Catch ex As Exception
            MsgBox("Could not create the shortcut" & Environment.NewLine & ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error)

            Return False
        End Try
    End Function

    Private Sub btn_upload_Click(sender As Object, e As EventArgs)
        ofd_selectedFile.ShowDialog()
        If (ofd_selectedFile.FileName <> "") Then
            Dim Response = UploadFile(ofd_selectedFile.FileName)
            ofd_selectedFile.FileName = ""
            MsgBox(Response)
        End If
    End Sub

    Private Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        My.Settings.Username = tbx_user.Text
        My.Settings.Password = tbx_password.Text
        My.Settings.UploadDir = tbx_uploaddir.Text
        My.Settings.AutoShare = cbx_autoshare.Checked

        MsgBox(ValidateCredentials())
    End Sub

    Private Sub btn_context_add_Click(sender As Object, e As EventArgs) Handles btn_context_add.Click
        If (My.Computer.Registry.CurrentUser.OpenSubKey("Software\Classes\*\shell\Upload to Heskcloud") Is Nothing) Then
            btn_context_add.Enabled = False
            btn_context_remove.Enabled = True

            My.Computer.Registry.CurrentUser.CreateSubKey("Software\Classes\*\shell\Upload to Heskcloud")
            My.Computer.Registry.CurrentUser.CreateSubKey("Software\Classes\*\shell\Upload to Heskcloud\command")

            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Classes\*\shell\Upload to Heskcloud", "Icon", Application.ExecutablePath, Microsoft.Win32.RegistryValueKind.String)
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Classes\*\shell\Upload to Heskcloud\command", "", Application.ExecutablePath + " ""%1""", Microsoft.Win32.RegistryValueKind.String)
        End If
    End Sub

    Private Sub btn_context_remove_Click(sender As Object, e As EventArgs) Handles btn_context_remove.Click
        If (My.Computer.Registry.CurrentUser.OpenSubKey("Software\Classes\*\shell\Upload to Heskcloud") IsNot Nothing) Then
            btn_context_remove.Enabled = False
            btn_context_add.Enabled = True

            My.Computer.Registry.CurrentUser.DeleteSubKeyTree("Software\Classes\*\shell\Upload to Heskcloud")
        End If
    End Sub

    Private Sub btn_sendto_add_Click(sender As Object, e As EventArgs) Handles btn_sendto_add.Click
        If Not (System.IO.File.Exists(GetFolderPath(SpecialFolder.ApplicationData) + "\Microsoft\Windows\SendTo\Heskcloud.lnk")) Then
            btn_sendto_add.Enabled = False
            btn_sendto_remove.Enabled = True

            CreateShortcut(Application.ExecutablePath, GetFolderPath(SpecialFolder.ApplicationData) + "\Microsoft\Windows\SendTo", "Heskcloud")
        End If
    End Sub

    Private Sub btn_sendto_remove_Click(sender As Object, e As EventArgs) Handles btn_sendto_remove.Click
        If (System.IO.File.Exists(GetFolderPath(SpecialFolder.ApplicationData) + "\Microsoft\Windows\SendTo\Heskcloud.lnk")) Then
            btn_sendto_remove.Enabled = False
            btn_sendto_add.Enabled = True

            System.IO.File.Delete(GetFolderPath(SpecialFolder.ApplicationData) + "\Microsoft\Windows\SendTo\Heskcloud.lnk")
        End If
    End Sub

    Private Sub lbl_uploaddir_Click(sender As Object, e As EventArgs) Handles lbl_uploaddir.Click
        If (My.Computer.Keyboard.ShiftKeyDown) Then
            frm_advanced.ShowDialog()
        End If
    End Sub
End Class