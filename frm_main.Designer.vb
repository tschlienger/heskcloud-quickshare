﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_main))
        Me.ofd_selectedFile = New System.Windows.Forms.OpenFileDialog()
        Me.lbl_password = New System.Windows.Forms.Label()
        Me.tbx_password = New System.Windows.Forms.TextBox()
        Me.lbl_user = New System.Windows.Forms.Label()
        Me.tbx_user = New System.Windows.Forms.TextBox()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.btn_context_remove = New System.Windows.Forms.Button()
        Me.lbl_sendto = New System.Windows.Forms.Label()
        Me.btn_context_add = New System.Windows.Forms.Button()
        Me.cbx_autoshare = New System.Windows.Forms.CheckBox()
        Me.btn_save = New System.Windows.Forms.Button()
        Me.lbl_uploaddir = New System.Windows.Forms.Label()
        Me.tbx_uploaddir = New System.Windows.Forms.TextBox()
        Me.btn_sendto_remove = New System.Windows.Forms.Button()
        Me.lbl_context = New System.Windows.Forms.Label()
        Me.btn_sendto_add = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_password
        '
        Me.lbl_password.AutoSize = True
        Me.lbl_password.Location = New System.Drawing.Point(6, 57)
        Me.lbl_password.Name = "lbl_password"
        Me.lbl_password.Size = New System.Drawing.Size(53, 13)
        Me.lbl_password.TabIndex = 16
        Me.lbl_password.Text = "Password"
        '
        'tbx_password
        '
        Me.tbx_password.Location = New System.Drawing.Point(6, 73)
        Me.tbx_password.Name = "tbx_password"
        Me.tbx_password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tbx_password.Size = New System.Drawing.Size(199, 20)
        Me.tbx_password.TabIndex = 15
        '
        'lbl_user
        '
        Me.lbl_user.AutoSize = True
        Me.lbl_user.Location = New System.Drawing.Point(6, 16)
        Me.lbl_user.Name = "lbl_user"
        Me.lbl_user.Size = New System.Drawing.Size(55, 13)
        Me.lbl_user.TabIndex = 14
        Me.lbl_user.Text = "Username"
        '
        'tbx_user
        '
        Me.tbx_user.Location = New System.Drawing.Point(6, 32)
        Me.tbx_user.Name = "tbx_user"
        Me.tbx_user.Size = New System.Drawing.Size(199, 20)
        Me.tbx_user.TabIndex = 13
        '
        'btn_context_remove
        '
        Me.btn_context_remove.Enabled = False
        Me.btn_context_remove.Location = New System.Drawing.Point(143, 19)
        Me.btn_context_remove.Name = "btn_context_remove"
        Me.btn_context_remove.Size = New System.Drawing.Size(62, 23)
        Me.btn_context_remove.TabIndex = 23
        Me.btn_context_remove.Text = "Remove"
        Me.btn_context_remove.UseVisualStyleBackColor = True
        '
        'lbl_sendto
        '
        Me.lbl_sendto.AutoSize = True
        Me.lbl_sendto.Location = New System.Drawing.Point(6, 53)
        Me.lbl_sendto.Name = "lbl_sendto"
        Me.lbl_sendto.Size = New System.Drawing.Size(81, 13)
        Me.lbl_sendto.TabIndex = 22
        Me.lbl_sendto.Text = "Send To Menu:"
        '
        'btn_context_add
        '
        Me.btn_context_add.Enabled = False
        Me.btn_context_add.Location = New System.Drawing.Point(96, 19)
        Me.btn_context_add.Name = "btn_context_add"
        Me.btn_context_add.Size = New System.Drawing.Size(41, 23)
        Me.btn_context_add.TabIndex = 21
        Me.btn_context_add.Text = "Add"
        Me.btn_context_add.UseVisualStyleBackColor = True
        '
        'cbx_autoshare
        '
        Me.cbx_autoshare.AutoSize = True
        Me.cbx_autoshare.Location = New System.Drawing.Point(6, 141)
        Me.cbx_autoshare.Name = "cbx_autoshare"
        Me.cbx_autoshare.Size = New System.Drawing.Size(185, 17)
        Me.cbx_autoshare.TabIndex = 20
        Me.cbx_autoshare.Text = "Automatically share uploaded files"
        Me.cbx_autoshare.UseVisualStyleBackColor = True
        '
        'btn_save
        '
        Me.btn_save.Location = New System.Drawing.Point(6, 161)
        Me.btn_save.Name = "btn_save"
        Me.btn_save.Size = New System.Drawing.Size(199, 23)
        Me.btn_save.TabIndex = 19
        Me.btn_save.Text = "Save and Validate"
        Me.btn_save.UseVisualStyleBackColor = True
        '
        'lbl_uploaddir
        '
        Me.lbl_uploaddir.AutoSize = True
        Me.lbl_uploaddir.Location = New System.Drawing.Point(6, 99)
        Me.lbl_uploaddir.Name = "lbl_uploaddir"
        Me.lbl_uploaddir.Size = New System.Drawing.Size(57, 13)
        Me.lbl_uploaddir.TabIndex = 18
        Me.lbl_uploaddir.Text = "Upload Dir"
        '
        'tbx_uploaddir
        '
        Me.tbx_uploaddir.Location = New System.Drawing.Point(6, 115)
        Me.tbx_uploaddir.Name = "tbx_uploaddir"
        Me.tbx_uploaddir.Size = New System.Drawing.Size(199, 20)
        Me.tbx_uploaddir.TabIndex = 17
        '
        'btn_sendto_remove
        '
        Me.btn_sendto_remove.Enabled = False
        Me.btn_sendto_remove.Location = New System.Drawing.Point(143, 48)
        Me.btn_sendto_remove.Name = "btn_sendto_remove"
        Me.btn_sendto_remove.Size = New System.Drawing.Size(62, 23)
        Me.btn_sendto_remove.TabIndex = 26
        Me.btn_sendto_remove.Text = "Remove"
        Me.btn_sendto_remove.UseVisualStyleBackColor = True
        '
        'lbl_context
        '
        Me.lbl_context.AutoSize = True
        Me.lbl_context.Location = New System.Drawing.Point(6, 24)
        Me.lbl_context.Name = "lbl_context"
        Me.lbl_context.Size = New System.Drawing.Size(76, 13)
        Me.lbl_context.TabIndex = 25
        Me.lbl_context.Text = "Context Menu:"
        '
        'btn_sendto_add
        '
        Me.btn_sendto_add.Enabled = False
        Me.btn_sendto_add.Location = New System.Drawing.Point(96, 48)
        Me.btn_sendto_add.Name = "btn_sendto_add"
        Me.btn_sendto_add.Size = New System.Drawing.Size(41, 23)
        Me.btn_sendto_add.TabIndex = 24
        Me.btn_sendto_add.Text = "Add"
        Me.btn_sendto_add.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lbl_sendto)
        Me.GroupBox1.Controls.Add(Me.lbl_context)
        Me.GroupBox1.Controls.Add(Me.btn_sendto_remove)
        Me.GroupBox1.Controls.Add(Me.btn_sendto_add)
        Me.GroupBox1.Controls.Add(Me.btn_context_remove)
        Me.GroupBox1.Controls.Add(Me.btn_context_add)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 217)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(211, 81)
        Me.GroupBox1.TabIndex = 27
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Integration"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lbl_user)
        Me.GroupBox2.Controls.Add(Me.tbx_user)
        Me.GroupBox2.Controls.Add(Me.btn_save)
        Me.GroupBox2.Controls.Add(Me.cbx_autoshare)
        Me.GroupBox2.Controls.Add(Me.tbx_password)
        Me.GroupBox2.Controls.Add(Me.lbl_password)
        Me.GroupBox2.Controls.Add(Me.tbx_uploaddir)
        Me.GroupBox2.Controls.Add(Me.lbl_uploaddir)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(211, 199)
        Me.GroupBox2.TabIndex = 28
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Connection and Behaviour"
        '
        'frm_main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(235, 309)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frm_main"
        Me.Text = "QS Configuration"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ofd_selectedFile As OpenFileDialog
    Friend WithEvents lbl_password As Label
    Friend WithEvents tbx_password As TextBox
    Friend WithEvents lbl_user As Label
    Friend WithEvents tbx_user As TextBox
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents btn_context_remove As Button
    Friend WithEvents lbl_sendto As Label
    Friend WithEvents btn_context_add As Button
    Friend WithEvents cbx_autoshare As CheckBox
    Friend WithEvents btn_save As Button
    Friend WithEvents lbl_uploaddir As Label
    Friend WithEvents tbx_uploaddir As TextBox
    Friend WithEvents btn_sendto_remove As Button
    Friend WithEvents lbl_context As Label
    Friend WithEvents btn_sendto_add As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
End Class
