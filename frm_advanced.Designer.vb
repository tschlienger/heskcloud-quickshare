﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_advanced
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_advanced))
        Me.tbx_davurl = New System.Windows.Forms.TextBox()
        Me.lbl_davurl = New System.Windows.Forms.Label()
        Me.lbl_fileshareapiurl = New System.Windows.Forms.Label()
        Me.tbx_fileshareapiurl = New System.Windows.Forms.TextBox()
        Me.btn_save = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'tbx_davurl
        '
        Me.tbx_davurl.Location = New System.Drawing.Point(12, 25)
        Me.tbx_davurl.Name = "tbx_davurl"
        Me.tbx_davurl.Size = New System.Drawing.Size(293, 20)
        Me.tbx_davurl.TabIndex = 0
        '
        'lbl_davurl
        '
        Me.lbl_davurl.AutoSize = True
        Me.lbl_davurl.Location = New System.Drawing.Point(12, 9)
        Me.lbl_davurl.Name = "lbl_davurl"
        Me.lbl_davurl.Size = New System.Drawing.Size(128, 13)
        Me.lbl_davurl.TabIndex = 1
        Me.lbl_davurl.Text = "Nextcloud WebDAV-URL"
        '
        'lbl_fileshareapiurl
        '
        Me.lbl_fileshareapiurl.AutoSize = True
        Me.lbl_fileshareapiurl.Location = New System.Drawing.Point(12, 48)
        Me.lbl_fileshareapiurl.Name = "lbl_fileshareapiurl"
        Me.lbl_fileshareapiurl.Size = New System.Drawing.Size(128, 13)
        Me.lbl_fileshareapiurl.TabIndex = 3
        Me.lbl_fileshareapiurl.Text = "Nextcloud WebDAV-URL"
        '
        'tbx_fileshareapiurl
        '
        Me.tbx_fileshareapiurl.Location = New System.Drawing.Point(12, 64)
        Me.tbx_fileshareapiurl.Name = "tbx_fileshareapiurl"
        Me.tbx_fileshareapiurl.Size = New System.Drawing.Size(293, 20)
        Me.tbx_fileshareapiurl.TabIndex = 2
        '
        'btn_save
        '
        Me.btn_save.Location = New System.Drawing.Point(12, 90)
        Me.btn_save.Name = "btn_save"
        Me.btn_save.Size = New System.Drawing.Size(96, 23)
        Me.btn_save.TabIndex = 4
        Me.btn_save.Text = "Save"
        Me.btn_save.UseVisualStyleBackColor = True
        '
        'frm_advanced
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(317, 123)
        Me.Controls.Add(Me.btn_save)
        Me.Controls.Add(Me.lbl_fileshareapiurl)
        Me.Controls.Add(Me.tbx_fileshareapiurl)
        Me.Controls.Add(Me.lbl_davurl)
        Me.Controls.Add(Me.tbx_davurl)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frm_advanced"
        Me.Text = "QS Advanced Configuration"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tbx_davurl As TextBox
    Friend WithEvents lbl_davurl As Label
    Friend WithEvents lbl_fileshareapiurl As Label
    Friend WithEvents tbx_fileshareapiurl As TextBox
    Friend WithEvents btn_save As Button
End Class
