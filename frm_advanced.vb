﻿Public Class frm_advanced
    Private Sub frm_advanced_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tbx_davurl.Text = My.Settings.DavUrl
        tbx_fileshareapiurl.Text = My.Settings.FileShareApiUrl
    End Sub

    Private Sub btn_save_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        My.Settings.DavUrl = tbx_davurl.Text
        My.Settings.FileShareApiUrl = tbx_fileshareapiurl.Text

        Me.Close()
    End Sub
End Class