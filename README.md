# Heskcloud QuickShare
## Description
This tool allows you to quickly upload files into a certain directory inside your Heskcloud account. You can also choose to instantly share uploaded files and copy the link to your clipboard.
There are three ways to do this:
- Drag & Drop a file onto the Heskcloud QuickShare application or a shortcut of it
- Add Heskcloud to your file context menu
- Add Heskcloud to your send to menu inside your file context menu
## How to install AND update
Just download the installer `Setup\heskcloud-qs-setup.exe` and execute it on your computer.